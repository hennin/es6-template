import del                from "del"
import gulp               from "gulp"
import gulpRename         from "gulp-rename"
import gulpUglify         from "gulp-uglify"
import { rollup }         from "rollup"
import rollupNodeResolve  from "rollup-plugin-node-resolve"
import rollupCommonjs     from "rollup-plugin-commonjs"
import rollupBabel        from "rollup-plugin-babel"

const gulpClean = (...args) => function clean() { return del(...args); };

gulp.task("build", gulp.series(
    async ()=> {
        const bundle = await rollup({
            entry: "src/main.es6",
            plugins: [
                rollupNodeResolve({ jsnext: true }),
                rollupCommonjs(),
                rollupBabel({
                    babelrc: false,
                    presets: ["es2015-rollup"],
                }),
            ],
        });

        return bundle.write({
            format: "iife",
            dest: "dist/es6-template.js",
            moduleName: "build"
        });
    },
    () => gulp
        .src(["dist/es6-template.js"])
        .pipe(gulpUglify())
        .pipe(gulpRename({
            extname: ".min.js"
        }))
        .pipe(gulp.dest("dist"))
));

gulp.task("default", gulp.series(
    gulpClean(["dist"]),
    gulp.task("build")
));
