import Human from './human.es6'
import { tarzan, aichan } from './shouts.es6'

let hennin = new Human('hennin')
hennin.run()
hennin.shout(tarzan())
hennin.shout(aichan())
