export default class Human {
  constructor(name) {
    this.name = name
    this.age = 20
  }

  run() {
    console.log(this.name + " is running!")
  }

  shout(words) {
    console.log(words + "!!!")
  }
}
